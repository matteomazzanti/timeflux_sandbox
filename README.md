# Tryout TimeFlux

Tryout and play with TimeFlux.


## Install TimeFlux

1. python virtual environemnt

```
$ python -m venv timeflux-sandbox 
$ source timeflux-sandbox/bin/activate
```

2. add more timeflux things
```
pip install timeflux_ui
```

## Launch a TimeFlux setup
```
$ timeflux ui.yaml
```
Browse http://localhost:8000
